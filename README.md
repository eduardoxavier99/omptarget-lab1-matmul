# OpenMP -target- Labs

Let's learn parallel programming using OpenMP!

## Matmul

Matrix multiplication or matrix product is a binary operation that produces a matrix from two matrices. The matrix product is designed for representing the composition of linear maps that are represented by matrices.

See [Wikipedia](https://en.wikipedia.org/wiki/Matrix_multiplication).

#### TODO:

1. Fork the repository to your own account.
2. Clone your forked repository on your computer.
3. Study the code using your favorite editor.
4. Parallelize the code on your CPU using OpenMP just like you did during the ompfor-lab2.
5. Add directives to run the parallel matrice mulltiplication on your gpu
6. Compile it using our [custom LLVM](https://gitlab.com/brcloud/brcloud-dev/wikis/Tutorial-LLVM-and-Clang) or the [official LLVM release](http://releases.llvm.org/download.html) (v8.0+). An additional compilation flag is required to compile the computational kernel for the GPU: `-fopenmp-targets=nvptx64-nvidia-cuda`, you need to add it to the CMake configuration file.
7. Profile the execution using `nvprof` to confirm that the kernel is running on the GPU.
8. Optimize the parallelization on the GPU using the `distribute` OpenMP clause.


Anything missing? Ideas for improvements? Make a pull request.
