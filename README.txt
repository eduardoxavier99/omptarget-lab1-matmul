Instalei o llvm e clang

Tentei mudar de compilador clang ou gcc

sudo update-alternatives --config cc

e mudar de compilador clang++ ou g++

sudo update-alternatives --config c++

porém não funcionou no cmake.

No cmake só funcionou assim, especificando o compilador
cmake -DCMAKE_C_COMPILER=/usr/bin/clang -DCMAKE_CXX_COMPILER=/usr/bin/clang++

Mas o make não funciona, aparentemente o clang com CUDA tem que configurar mais coisas https://llvm.org/docs/CompileCudaWithLLVM.html

Para usar o gcc mesmo, ao invés da flag no CMakeLists
-fopenmp-targets=nvptx64-nvidia-cuda
usamos
-omptargets=nvptx64sm_35-nvidia-linux


Tentei diretamente compilar o codigo com
 clang++ mat-mul.c -o mat -fopenmp -fopenmp-targets=nvptx64-nvidia-cuda -L/usr/local/cuda/lib64 
mas nao funciona
